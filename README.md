# SARS-CoV-2 Database

Highly curated SARS Covid19 contextual database.

Please visit our [online service](https://covid19.sfb.uit.no/) where you can access the database.

For administrators, use [this URL](https://covid19.sfb.uit.no/wp-admin).

## API

A [REST API](https://databasesapi.sfb.uit.no/) give programmatic access to the data.
For more details, see [here](https://gitlab.com/uit-sfb/cbf/-/wikis/Design/REST-API).

## Documentation

Please, visit our [Wiki pages](https://gitlab.com/uit-sfb/cbf/-/wikis/home) to know more about the project.

## Our technology

SARS-CoV-2 database is based on the opensource [CBF project](https://gitlab.com/uit-sfb/cbf) (Contextual Biodata Framework),
providing researchers with a simple way to store, retrieve, analyse, visualize and link data! Please visit the [project page](https://gitlab.com/uit-sfb/cbf) for more details.