require 'erb'

# Cf https://raw.githubusercontent.com/wurmlab/sequenceserver/master/lib/sequenceserver/links.rb
# The links defined here are added to the ones defined in the original links.rb above

module SequenceServer
  # Module to contain methods for generating sequence retrieval links.
  module Links
    # Provide a method to URL encode _query parameters_. See [1].
    include ERB::Util
    alias encode url_encode

    ENA_ID_PATTERN = /([A-Z]{2}\d{6})/

    def covid19
      return nil unless id.match(ENA_ID_PATTERN)# or title.match(ENA_ID_PATTERN)
      ena_acc = Regexp.last_match[1]
      ena_acc = encode ena_acc
      url = "https://identifiers.org/covid19:SFB_COVID19_#{ena_acc}"
      {
        order: 2,
        title: 'SARS-CoV-2',
        url:   url,
        icon:  'fa-external-link'
      }
    end

    def ena
      return nil unless id.match(ENA_ID_PATTERN)# or title.match(ENA_ID_PATTERN)
      ena_acc = Regexp.last_match[1]
      ena_acc = encode ena_acc
      url = "https://www.ebi.ac.uk/ena/browser/view/#{ena_acc}"
      {
        order: 2,
        title: 'ENA',
        url:   url,
        icon:  'fa-external-link'
      }
    end
  end
end

# [1]: https://stackoverflow.com/questions/2824126/whats-the-difference-between-uri-escape-and-cgi-escape