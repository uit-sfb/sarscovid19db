#!/usr/bin/env bash
set -e
POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
      -h|--help)
      echo "Builds BLAST db"
      echo ""
      echo "Usage"
      echo "-i <inputDir> -o <outputDir> <version>"
      echo "-i|--input-dir: Path to input directory"
      echo "-o|--output-dir: Path to output directory"
      echo "version: DB version (without 'V')"
      echo ""
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
      -i|--input-dir)
      INPUT_DIR=$2
      shift
      shift
      ;;
      -o|--output-dir)
      OUTPUT_DIR=$2
      shift
      shift
      ;;
      *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

if [[ -z ${POSITIONAL[0]} ]]; then
  echo "Please provide which db version to build"
  exit 1
else
  VERSION=${POSITIONAL[0]}
fi

mkdir -p $OUTPUT_DIR
rm -rf $OUTPUT_DIR/*

#The input files need to be validated to keep makeblastdb happy
set -x
seqkit seq -t dna $INPUT_DIR/sars_covid19_${VERSION}_assembly.fa > /tmp/validated.fa
seqkit seq -t dna $INPUT_DIR/sars_covid19_${VERSION}_cds.fna > /tmp/validated.fna
seqkit seq -t protein $INPUT_DIR/sars_covid19_${VERSION}_protein.faa > /tmp/validated.faa

/usr/local/lib/blast+/bin/makeblastdb -in /tmp/validated.fa -out $OUTPUT_DIR/SARS-CoV-2_${VERSION}_assembly -title "SARS-CoV-2 $VERSION -- Genome assemblies" -dbtype nucl -parse_seqids
/usr/local/lib/blast+/bin/makeblastdb -in /tmp/validated.fna -out $OUTPUT_DIR/SARS-CoV-2_${VERSION}_cds -title "SARS-CoV-2 $VERSION -- CDS Nucleotide" -dbtype nucl -parse_seqids
/usr/local/lib/blast+/bin/makeblastdb -in /tmp/validated.faa -out $OUTPUT_DIR/SARS-CoV-2_${VERSION}_protein -title "SARS-CoV-2 $VERSION -- CDS Protein" -dbtype prot -parse_seqids
